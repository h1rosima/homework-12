//import prettier from 'prettier';

document.addEventListener('keypress', function (event) {
    const pressedKey = event.key.toUpperCase();
    const btnS = document.querySelectorAll('.btn');

    btnS.forEach(function (button) {
        if (button.textContent.toUpperCase() === pressedKey) {
            button.style.backgroundColor = 'blue';
        } else {
            button.style.backgroundColor = 'black';
        }
    });
});
